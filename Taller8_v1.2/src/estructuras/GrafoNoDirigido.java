package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> {

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private Lista listN;

	/**
	 * Lista de adyacencia 
	 */
	private HashMap<String, List<Arco<K,E>>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
		// Es libre de implementarlo con la representacion de su agrado. 
	private Lista listas; 
	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		//TODO implementar

		listN = new Lista();
		listas = new Lista();
	}

	@Override
	public boolean agregarNodo(Nodo<K> nodo) {
		//TODO implementar
		
		int c = 0;
		boolean ret = true;
		for (int i = 0 ; i < listN.size(); i++)
		{
			if(((Nodo)listN.get(i)).darId() == nodo.darId())
			{
				return false;
			}
			
		}
		listN.add(nodo);
		return true;
		
	}

	@Override
	public boolean eliminarNodo(K id) {
		//TODO implementar

		for (int i = 0 ; i < listN.size(); i++)
		{
			
			if(((Nodo)listN.get(i)).darId() == id)
			{
				listN.remove(i);
				return true;
			}
			
		}
		
		return false;
	}

	@Override
	public Arco<K,E>[] darArcos() {
		//TODO implementar
		Arco[] arc = new Arco[listas.size()];
		
		for (int i = 0 ; i < listas.size(); i++)
		{
			arc[i] = (Arco)listas.get(i);
			
		}
		
		return arc;
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		{
			return null;
		}
	}

	@Override
	public Nodo<K>[] darNodos() {
		//TODO implementar
		Nodo[]  nod= new Nodo[listas.size()];
		
		for (int i = 0 ; i < listN.size(); i++)
		{
			nod[i] = (Nodo)listN.get(i);
			
		}
		
		return nod;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj) {
		//TODO implementar
		Arco a = crearArco(inicio, fin, costo, obj);
		
		for (int i = 0; i< listas.size(); i++)
		{
			Arco b = (Arco) listas.get(i);
			Nodo in = (Nodo)b.darNodoInicio();
			Nodo f = (Nodo)b.darNodoFin();
			Nodo ii = (Nodo)a.darNodoInicio();
			Nodo ff = (Nodo)a.darNodoFin();
			if(in.darId().equals(ii.darId()) && f.darId().equals(ff.darId()))
			{
				return false;
						
			}
			
	
		}
		
		listas.add(a);
		return true;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) {
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin) {
		//TODO implementar
	Arco ret = null;
		
		for (int i = 0 ; i < listas.size(); i++)
		{
			Arco a = (Arco)listas.get(i);
			Nodo in = a.darNodoInicio();
			Nodo f = a.darNodoFin();
			if(in.darId()==inicio && f.darId() == fin)
			{
				ret = (Arco)listas.get(i);
				listas.remove(i);
			
			}
			
		}
		return ret;
	}

	@Override
	public Nodo<K> buscarNodo(K id) {
		//TODO implementar
		Nodo ret = null;
		for (int i = 0 ; i < listN.size(); i++)
		{
			if(((Nodo)listN.get(i)).darId() == id)
			{
				ret =(Nodo) listN.get(i);
			}
			
		}
		
		return ret;
	}
	

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) {
		//TODO implementar
		Arco[] ret = new Arco[listas.size()];
		
		int con = 0;
		for (int i = 0 ; i < listN.size(); i++)
		{
			Arco a = (Arco)listas.get(i);
			Nodo in = a.darNodoInicio();
			if(id == in.darId() )
			{
				ret[con] = a;
			}
			
		}
		
		return ret;
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) {
		//TODO implementar
Arco[] ret = new Arco[listas.size()];
		
		int con = 0;
		for (int i = 0 ; i < listN.size(); i++)
		{
			Arco a = (Arco)listas.get(i);
			Nodo in = a.darNodoFin();
			if(id == in.darId() )
			{
				ret[con] = a;
			}
			
		}
		
		return ret;
	}

}
